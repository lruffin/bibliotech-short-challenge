# BIBLIOTECH SHORT CHALLENGE

- [x] Firstly download the Polymer Starter Kit (https://github.com/PolymerElements/polymer-starter-kit)
- [x] Use the 'polymer serve' command to start the development server
- [x] Replace one of the views with a new element which displays the overview of the title
- [x] Use https://d1re4mvb3lawey.cloudfront.net/pg1017/cover.jpg as the image for the book cover using iron-image https://www.webcomponents.org/element/PolymerElements/iron-image
- [x] Use an iron-ajax (https://www.webcomponents.org/element/PolymerElements/iron-ajax) to request the books meta-information from https://d1re4mvb3lawey.cloudfront.net/pg1017/index.json snf display on the page, would suggest using the title, contributors and publisher information.
- (opt) Display the table of contents with the title as well
- (opt) Make all the components modular so the isbn can be switched to another book